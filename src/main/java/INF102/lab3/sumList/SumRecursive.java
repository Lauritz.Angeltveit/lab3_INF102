package INF102.lab3.sumList;

import java.util.HashMap;
import java.util.List;


public class SumRecursive implements ISum {
    HashMap<Long, Long> memory = new HashMap<Long, Long>();
    
    @Override
    public long sum(List<Long> list) {
        if (list.size() == 0) { //O(1)
            return 0;
        }
        else { //O(1)
            return list.get(0) + sum(list.subList(1, list.size()));
        }
    }
    

}
