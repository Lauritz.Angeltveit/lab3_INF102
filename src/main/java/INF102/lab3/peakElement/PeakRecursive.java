package INF102.lab3.peakElement;


import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers.isEmpty())
             throw new IllegalArgumentException("Empty list");
        if(numbers.size() == 1)
            return numbers.get(0);
        if (numbers.get(0) >= numbers.get(1)) 
            return numbers.get(0);
        else
            return peakElement(numbers.subList(1, numbers.size()));
        
        

        // Denne funker også, lengere kjøretid
        // Collections.sort(numbers);
        // int n = numbers.size();
        // return numbers.get(n-1);
        
        //Kladd
        // int n = numbers.size();
        // int mid = n/2;

        // if (numbers.isEmpty())
        //     throw new IllegalArgumentException("Empty list");
        
        // if (n == 1)
        //     return numbers.get(0);
        
        // if (numbers.get(0) > numbers.get(1))
        //     return numbers.get(0);

        // if (numbers.get(n-1) > numbers.get(n-2))
        //     return numbers.get(n-1);

        // //Sjekker om midten er peak
        // if (numbers.get(mid) > numbers.get(mid + 1) && numbers.get(mid) > numbers.get(mid - 1)){
        //     return numbers.get(mid);
        // }
        // //Hvis tallet til høyre er større, vil peaken være på høyre side
        // else if(numbers.get(mid) < numbers.get(mid + 1)){
        //     return peakElement(numbers.subList(mid+1, n-2));
        // } 
        // //Ellers er peaken på venstre side
        // else {
        //     return peakElement(numbers.subList(1, mid-1));
        // }
    }

}
